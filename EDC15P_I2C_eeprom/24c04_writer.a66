; Copyright (c) 2014, JohnJocke
; BSD license, check file LICENSE

$SEGMENTED
$CASE

$IF __MOD167__
$INCLUDE (REG167.INC)
$ENDIF

PUBLIC serinit4
ASSUME	DPP3:SYSTEM

?PR?SERINIT4	section	code 'NCODE'

serinit4	proc	near
	BCLR S0RIR
	MOV R0, #057H
	CALLR send_byte
	
	;initialize data and clock lines
	EXTR #2
	BSET ODP2.9 ; open drain
	BSET ODP2.8 ; open drain
	BSET DP2.9 ; SCL
	BSET DP2.8 ; SDA
	BSET SDA ; idle high
	BSET SCL ; idle high
	
	MOV R0, #058H
	CALLR send_byte
	
	MOV R8, #0
	MOV R4, #0F6A0H
rx_bytes:
	CALLR read_byte
	MOVB [R4], RL0
	ADD R4, #1
	CMP R4, #0F6B0H
	JMP cc_NE, rx_bytes
	
	MOV R4, #0F6A0H
	CALLR eeprom_write
	CALLR send_byte
	
	MOV R4, #0F6A0H	
	ADD R8, #10H
	CMP R8, #0200H
	JMP cc_NE, rx_bytes

	JMPA cc_UC, 0F600H ; return to 2nd_loader

; ---- Function read_byte, read byte from serial port -----
read_byte:
	JNB	S0RIR, read_byte
	BCLR S0RIR
	MOV R0, S0RBUF
	
	RET
	
; ---- Function send_byte, send byte to serial port -----
send_byte:
	MOV S0TBUF, R0
wait_tx:
	JNB	S0RIR, wait_tx  ; KLINE receives echo
	BCLR S0RIR
	
	RET
	
; ---- Function eeprom_write, write bytes to eeprom -----
eeprom_write:
	MOV R0, #0ACH
	BMOV R0.1, R8.8
	CALLR i2c_start
	CALLR i2c_send
	CMP R3, #0
	JMP	cc_NE, eeprom_write_error
	
	MOV R0, R8 ;write address memory
	CALLR i2c_send
	CMP R3, #0
	JMP	cc_NE, eeprom_write_error
			
	MOV R5, #10H ; 16 bytes to write
eeprom_write_loop:
	MOVB RL0, [R4]
	CALLR i2c_send
	CMP R3, #0
	JMP	cc_NE, eeprom_write_error
	ADD R4, #1
	SUB R5, #1
	CMP R5, #0
	JMP cc_NE, eeprom_write_loop

	CALLR i2c_start	; TODO: should need only stop (something is wrong with i2c_stop)
	CALLR i2c_stop
	MOV R0, #058H
	RET
	
eeprom_write_error:
	CALLR i2c_stop
	MOV R0, #036H
	RET
	
; ---- Function i2c_read, i2c read byte -----	
i2c_read:
	MOV R0, #0
	MOV R2, #8
	BCLR DP2.8 ; SDA
rx_byte_loop:
	MOV R6, #2
	CALLR q_delay
	BSET SCL
	MOV R6, #1
	CALLR q_delay	
	SHL R0, #1
	BMOV R0.0, SDA
	MOV R6, #1
	CALLR q_delay
	BCLR SCL
	SUB R2, #1
	CMP	R2,#0
	JMP	cc_NE, rx_byte_loop
	
	MOV R6, #1
	CALLR q_delay	
	
	BSET DP2.8 ; SDA
	BMOV SDA, R3.0
	
	MOV R6, #1
	CALLR q_delay
	BSET SCL
	MOV R6, #2
	CALLR q_delay
	BCLR SCL
	RET
	
; ---- Function i2c_send, i2c send byte -----	
i2c_send:
	MOV R3, #0
	MOV R2, #8
	MOV R6, #1
	CALLR q_delay
tx_byte_loop:
	BMOV SDA, R0.7
	MOV R6, #1
	CALLR q_delay
	BSET SCL
	MOV R6, #2
	CALLR q_delay	
	BCLR SCL
	MOV R6, #1
	CALLR q_delay
	SHL R0, #1
	SUB R2, #1
	CMP	R2,#0
	JMP	cc_NE, tx_byte_loop
	
	;read ack/nack
	MOV R6, #1
	CALLR q_delay
	BSET SCL
	BCLR DP2.8 ; SDA
	
	MOV R6, #1
	CALLR q_delay
	BMOV R3.0, SDA
	MOV R6, #1
	CALLR q_delay
	
	BCLR SCL
	BSET DP2.8 ; SDA
	RET
	
; ---- Function i2c_start, i2c bus start condition -----	
i2c_start:
	MOV R6, #1
	CALLR q_delay
	BCLR SDA
	MOV R6, #1
	CALLR q_delay
	BCLR SCL
	RET
	
; ---- Function i2c_stop, i2c bus stop condition -----
i2c_stop:
	MOV R6, #2
	CALLR q_delay
	BSET SCL
	MOV R6, #1
	CALLR q_delay
	BSET SDA
	RET
	
	
; ---- Function q_delay, quarter of an i2c clock cycle -----
q_delay:
	MOV	R7, #30 ;30 ~8khz
del_loop:
	SUB	R7, #1
	CMP	R7, #0
	JMP	cc_NE, del_loop
	SUB	R6, #1
	CMP	R6, #0
	JMP	cc_NE, q_delay
	RET
		

serinit4	endp

?PR?SERINIT4	ENDS

	end
