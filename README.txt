This program can read and write the immobilizer eeprom of Bosch EDC15P+ ECU. You have to start the ECU in boot mode and you need a dumb KKL cable.

The program was tested with EDC15P+. I don't know if it will work with other EDC15 variants.

The eeprom is 24c04 which uses I2C bus. The eeprom is connected to MCU pins:
P2.8 - SDA
P2.9 - SCL

The address pins of the eeprom are high and therefore the chip responds to address: 0xAC

Bitbanging I2C drivers were written in C167 assembly to read/write the eeprom chip because the eeprom chip is connected to regular GPIO pins.

Folders:
- EDC15_Immo_flasher: contains source code for the .NET (C#) Windows program that communicates with the EDC15 ECU. The program loads bootloaders and the I2C drivers to ECU and processes eeprom image binaries.
- EDC15_I2C_eeprom: contains source codes for C167 assembly written I2C read and write programs (+ bootloaders). Use Keil uVision free version to compile and copy the binary code with hex editor from compiled .OBJ files.

All source codes are included with BSD license.