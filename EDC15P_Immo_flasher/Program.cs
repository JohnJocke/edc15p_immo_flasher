﻿/* Copyright (c) 2014, JohnJocke
 * BSD license, check file LICENSE
 */

using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;

namespace EDC15_Immo_flasher
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }
    }
}
