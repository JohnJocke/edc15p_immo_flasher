﻿/* Copyright (c) 2014, JohnJocke
 * BSD license, check file LICENSE
 */

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Threading;
using System.IO.Ports;
using System.IO;

namespace EDC15_Immo_flasher
{
    public class LogEntryEventArgs : EventArgs
    {
        private readonly string message;

        public LogEntryEventArgs(string arg)
        {
            this.message = arg;
        }
        public string Message
        {
            get { return this.message; }
        }
    }

    public partial class Form1 : Form
    {
        private static SerialPort m_port = new SerialPort();

        private static string m_portname;
        private static bool m_running = false;
        private static bool m_read = false;
        private static bool m_write = false;

        private static string m_readname = "";
        private static string m_writename = "";

        private delegate void LogText(object sender, LogEntryEventArgs e);
        private static event LogText LogEvent;

        private static int m_write_idx = 0;

        Thread m_thread;

        public Form1()
        {
            InitializeComponent();

            string[] ports= SerialPort.GetPortNames();
            Array.Sort<string>(ports);  
            comboBoxCOMPort.Items.AddRange(ports);
            comboBoxCOMPort.SelectedItem = ports[0];

            LogEvent += new LogText(LogHandler);
            richTextBoxLog.AppendText("Start ECU in boot mode and Connect. Then Read/Write EEPROM." + Environment.NewLine);
        }

        private void LogHandler(object sender, LogEntryEventArgs e)
        {
            //run on UI thread
            this.Invoke((MethodInvoker)delegate
            {
                richTextBoxLog.AppendText(e.Message + Environment.NewLine);
                richTextBoxLog.SelectionStart = richTextBoxLog.Text.Length;
                richTextBoxLog.ScrollToCaret();
            });
        }

        private void buttonConnectBoot_Click(object sender, EventArgs e)
        {
            if (m_running)
            {
                m_running = false;
            }
            else
            {
                m_portname = (string)comboBoxCOMPort.SelectedItem;
                m_thread = new Thread(new ThreadStart(SerialThread));
                m_thread.Start();
            }
        }

        public static void SerialThread()
        {
            try
            {
                m_port.Encoding = Encoding.GetEncoding("ISO-8859-1");
                m_port.BaudRate = 9600;
                m_port.PortName = m_portname;
                m_port.ReceivedBytesThreshold = 1;
                m_port.ReadTimeout = 500;
                m_port.WriteTimeout = 500;
                m_port.Open();
                m_port.Handshake = Handshake.None;
            }
            catch (Exception E)
            {
                LogEvent(null, new LogEntryEventArgs("Serial port not available!"));
                return;
            }

            try
            {
                //wakeup byte
                Write(0x00);
                LogEvent(null, new LogEntryEventArgs("Received CPU ID: " + Read().ToString("X2")));
                

                byte[] file1 = File.ReadAllBytes("1st_loader.bin");
                uploadFile(file1);

                LogEvent(null, new LogEntryEventArgs("Uploaded 1st stage loader, size: " + file1.Length.ToString("X2")));
                LogEvent(null, new LogEntryEventArgs("Received 1st loader ID: " + Read().ToString("X2")));

                byte[] file2 = File.ReadAllBytes("2nd_loader.bin");
                uploadFile(file2);

                LogEvent(null, new LogEntryEventArgs("Uploaded 2nd stage loader, size: " + file2.Length.ToString("X2")));
            }
            catch(Exception E)
            {
                LogEvent(null, new LogEntryEventArgs("Could not connect ECU! Check that ECU is in boot mode."));
                m_port.Close();
                return;
            }

            m_read = false;
            m_write = false;
            m_running = true;
            while (m_running)
            {
                if (m_read || m_write)
                {
                    byte[] file3 = null;

                    if (m_read)
                    {
                        file3 = File.ReadAllBytes("24c04_reader.bin");
                    }
                    else if (m_write)
                    {
                        file3 = File.ReadAllBytes("24c04_writer.bin");
                        m_write_idx = 0;
                    }

                    m_read = false;
                    m_write = false;

                    int addr = (file3.Length >> 8) & 0xFF;
                    Write((byte)addr);

                    addr = file3.Length & 0xFF;
                    Write((byte)addr);

                    uploadFile(file3);
                    LogEvent(null, new LogEntryEventArgs("Uploaded program, size: " + file3.Length.ToString("X2")));
                }

                try
                { 
                    byte rxbyte = Read();
                    //LogEvent(null, new LogEntryEventArgs("Received byte: " + rxbyte.ToString("X2")));
                    switch (rxbyte)
                    {
                        case 0x55:
                            LogEvent(null, new LogEntryEventArgs("Loader ready, please choose action..."));
                            break;
                        case 0x56:
                            LogEvent(null, new LogEntryEventArgs("Starting to read EEPROM data..."));

                            byte[] array = new byte[512];
                            for (int i = 0; i < 512; ++i)
                            {
                               rxbyte = Read();
                               array[i] = rxbyte;
                            }

                            LogEvent(null, new LogEntryEventArgs("EEPROM data read finished!"));

                            if (m_readname != "")
                            {
                                File.WriteAllBytes(m_readname, array);
                            }
                            else
                            {
                                LogEvent(null, new LogEntryEventArgs("WARNING: File name to save was not chosen..."));
                            }
                            m_read = false;
                            m_write = false;

                            break;
                        case 0x57:
                            LogEvent(null, new LogEntryEventArgs("Starting to write EEPROM data..."));
                            break;
                        case 0x58:
                            if (m_writename == "")
                            {
                                LogEvent(null, new LogEntryEventArgs("WARNING: Binary to write was not chosen. Boot ECU and try again..."));
                                break;
                            }

                            //TODO, not good to read the whole file every time...
                            byte[] file4 = File.ReadAllBytes(m_writename);

                            int j = 0;
                            for ( j = m_write_idx; j < m_write_idx + 16; ++j)
                            {
                                Write(file4[j]);
                            }
                            m_write_idx = j;

                            LogEvent(null, new LogEntryEventArgs("EEPROM data written: " + m_write_idx + "/" + file4.Length));

                            if (m_write_idx >= file4.Length)
                            {
                                LogEvent(null, new LogEntryEventArgs("EEPROM data write finished!"));
                                m_read = false;
                                m_write = false;
                            }
                            break;

                    }
                }
                catch(Exception E)
                {
                    continue; //timeout, try again
                }

            }

            m_port.Close();
            LogEvent(null, new LogEntryEventArgs("Closing ECU connection..."));
        }

        private string OpenFileDialog()
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();

            openFileDialog1.InitialDirectory = Application.StartupPath;
            openFileDialog1.Filter = "All files (All files (*.*)|*.*" ;
            openFileDialog1.FilterIndex = 1 ;
            openFileDialog1.RestoreDirectory = true ;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                return openFileDialog1.FileName;
            }

            return "";
        }

        private string SaveFileDialog()
        {
            SaveFileDialog openFileDialog1 = new SaveFileDialog();

            openFileDialog1.InitialDirectory = Application.StartupPath;
            openFileDialog1.Filter = "All files (All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 1;
            openFileDialog1.RestoreDirectory = true;

            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                return openFileDialog1.FileName;
            }

            return "";
        }

        private static void uploadFile(byte[] data)
        {
            for(int i = 0; i < data.Length; ++i)
            {
                Write(data[i]);
            }
        }

        private static byte Read()
        {
            byte[] rxbyte = new byte[1];
            m_port.Read(rxbyte, 0, 1);

            return rxbyte[0];
        }

        private static void Write(byte b)
        {
            byte[] b2send = new byte[1];
            byte[] rxbyte = new byte[1];
            b2send[0] = b;

            if (!m_port.IsOpen)
            {
                return;
            }

            m_port.Write(b2send, 0, 1);
            m_port.Read(rxbyte, 0 , 1);

            if (b2send[0] != rxbyte[0])
            {
                LogEvent(null, new LogEntryEventArgs("Wrong echo byte! " + rxbyte[0].ToString("X2")));
            }
        }

        private void buttonUpload_Click(object sender, EventArgs e)
        {
            if (!m_running)
                return;
            m_readname = SaveFileDialog();
            m_read = true;
        }

        private void buttonWrite_Click(object sender, EventArgs e)
        {
            if (!m_running)
                return;
            m_writename = OpenFileDialog();
            m_write = true;

        }
    }
}
