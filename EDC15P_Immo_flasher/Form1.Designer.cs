﻿namespace EDC15_Immo_flasher
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonConnectBoot = new System.Windows.Forms.Button();
            this.buttonUpload = new System.Windows.Forms.Button();
            this.buttonWrite = new System.Windows.Forms.Button();
            this.richTextBoxLog = new System.Windows.Forms.RichTextBox();
            this.comboBoxCOMPort = new System.Windows.Forms.ComboBox();
            this.SuspendLayout();
            // 
            // buttonConnectBoot
            // 
            this.buttonConnectBoot.Location = new System.Drawing.Point(53, 38);
            this.buttonConnectBoot.Name = "buttonConnectBoot";
            this.buttonConnectBoot.Size = new System.Drawing.Size(75, 38);
            this.buttonConnectBoot.TabIndex = 0;
            this.buttonConnectBoot.Text = "Connect";
            this.buttonConnectBoot.UseVisualStyleBackColor = true;
            this.buttonConnectBoot.Click += new System.EventHandler(this.buttonConnectBoot_Click);
            // 
            // buttonUpload
            // 
            this.buttonUpload.Location = new System.Drawing.Point(368, 38);
            this.buttonUpload.Name = "buttonUpload";
            this.buttonUpload.Size = new System.Drawing.Size(75, 38);
            this.buttonUpload.TabIndex = 1;
            this.buttonUpload.Text = "Read EEPROM";
            this.buttonUpload.UseVisualStyleBackColor = true;
            this.buttonUpload.Click += new System.EventHandler(this.buttonUpload_Click);
            // 
            // buttonWrite
            // 
            this.buttonWrite.Location = new System.Drawing.Point(467, 38);
            this.buttonWrite.Name = "buttonWrite";
            this.buttonWrite.Size = new System.Drawing.Size(75, 38);
            this.buttonWrite.TabIndex = 2;
            this.buttonWrite.Text = "Write EEPROM";
            this.buttonWrite.UseVisualStyleBackColor = true;
            this.buttonWrite.Click += new System.EventHandler(this.buttonWrite_Click);
            // 
            // richTextBoxLog
            // 
            this.richTextBoxLog.Location = new System.Drawing.Point(12, 108);
            this.richTextBoxLog.Name = "richTextBoxLog";
            this.richTextBoxLog.Size = new System.Drawing.Size(550, 258);
            this.richTextBoxLog.TabIndex = 3;
            this.richTextBoxLog.Text = "";
            // 
            // comboBoxCOMPort
            // 
            this.comboBoxCOMPort.FormattingEnabled = true;
            this.comboBoxCOMPort.Location = new System.Drawing.Point(151, 48);
            this.comboBoxCOMPort.Name = "comboBoxCOMPort";
            this.comboBoxCOMPort.Size = new System.Drawing.Size(73, 21);
            this.comboBoxCOMPort.TabIndex = 4;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(574, 378);
            this.Controls.Add(this.comboBoxCOMPort);
            this.Controls.Add(this.richTextBoxLog);
            this.Controls.Add(this.buttonWrite);
            this.Controls.Add(this.buttonUpload);
            this.Controls.Add(this.buttonConnectBoot);
            this.Name = "Form1";
            this.Text = "EDC15 Immo flasher";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonConnectBoot;
        private System.Windows.Forms.Button buttonUpload;
        private System.Windows.Forms.Button buttonWrite;
        private System.Windows.Forms.RichTextBox richTextBoxLog;
        private System.Windows.Forms.ComboBox comboBoxCOMPort;

    }
}

